
let start = new Date('2020-01')
let end = new Date('2020-12')

let results = [
    {
        "date" : new Date("2020-09"),
        "total" : 1.0
    }
].sort((a,b ) => a.date - b.date)

let idx = 0;

while(start < end){

    let obj = {
        date: start,
        total: 0
    }

    let result = results[idx]

    if(result && result.date.getMonth() == start.getMonth())
    {
        console.log('Start M\n', start, 'RESULT\n', result)
        obj = result
        ++idx
    }

    //console.log(obj)

    start.setMonth(start.getMonth() + 1)
}