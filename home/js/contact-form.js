// JavaScript Document
$(document).ready(function() {
    var contactTimeout = null

    let fnMessage = function(message){
        let ele = document.querySelector('.form-notification')
        ele.innerHTML = message

        if(contactTimeout)
            clearTimeout(contactTimeout)

        contactTimeout = setTimeout(() => {
            ele.innerHTML = ""
        }, 2000)
    }

    let element = document.querySelector('input.btn.btn-green.black-hover')
    let elements = document.querySelectorAll('.newsletter-form input[type="email"], .newsletter-form input[type="text"]')

    element.addEventListener('click', (evt) => {
        evt.preventDefault()

        let invalid = false

        for(let element of elements)
        {
            if((element.value + "").length == 0){
                invalid = true
                break;
            }
        }

        if(invalid)
            return fnMessage('Por favor preencha todos os campos')

        jQuery.ajax({
            method: "POST",
            url: "https://api.service.nplay.com.br/v1/user/contact",
            dataType: 'json',
            "headers": {
                "content-type": "application/json"
            },
            data: JSON.stringify( {
                fullName: document.querySelector('.newsletter-form input#fullName').value,
                phone: document.querySelector('.newsletter-form input#phone').value,
                email: document.querySelector('.newsletter-form input#email').value,
                url: document.querySelector('.newsletter-form input#url').value,
            }),
            success: function(response)
            {
                let msg = response.success ? 'Enviado com sucesso, obrigado!' : 'Falha ao enviar seu pedido'
                fnMessage(msg)
            },
            error: function(error)
            {
                fnMessage('Falha ao enviar seu pedido')
            }
        })
    })
})