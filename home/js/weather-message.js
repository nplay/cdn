var fnHideSection = function(){
    let section = document.querySelector('#weather-section')
    section.classList.add('hide')
}

var getIp = async function(){
    var requestOptions = {
        method: 'GET'
      };
      
      let response = await fetch("https://api.ipify.org", requestOptions)
      return response.text()
}

var fngetWeather = async function(session){
    var message = document.querySelector('#weather-section #weather-message')

    let temperatures = [
        (temp) => { return temp > 29 ? 'Muito calor' : null},
        (temp) => { return temp >= 26 ? 'Calor' : null},
        (temp) => { return temp >= 20 ? 'Um pouco frio' : null},
        (temp) => { return temp >= 10 ? 'Frio' : null},
        (temp) => { return temp <= 9 ? 'Muito frio' : null}
    ]

    let timeValid = new Date()
    timeValid = timeValid.setHours(timeValid.getHours() -2)

    jQuery.ajax({
        method: "POST",
        url: "https://history.service.nplay.com.br/v1/weather/byIP",
        dataType: 'json',
        "headers": {
            "content-type": "application/json",
            'Authorization-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkb21haW5zIjoiaHR0cHM6Ly9sb2phdGVzdGUuYmlzd3MuY29tLmJyLyIsInNjb3BlIjoidXNlci5wYXRoIiwicGxhdGZvcm0iOiJiaXMyYmlzIiwiZElkIjoiMTU5NzI0MzMyNDM3MSIsImlhdCI6MTU5NzI0MzMyNH0.ks4OAnCLRdMuixaW_SSv11UUcr8vB--tvsWRYTerpjg'
        },
        data: JSON.stringify({
            "ip": await getIp(),
            "gteDate": new Date(timeValid).toString()
        }),
        success: function(response)
        {
            if(response.success == false)
            {
                console.log(response)
                fnHideSection()
                return
            }

            let fnWord = temperatures.find( fn => {
                return fn(response.data.temp)
            })

            message.innerHTML = `${fnWord(response.data.temp)} ai em ${response.data.city} hoje?`
        },
        error: function(error)
        {
            console.log(error)
            fnHideSection()
        }
    })
}