#NGINX BUILD
FROM debian:buster-slim

RUN apt-get update

RUN apt-get install wget -y
RUN wget "http://nginx.org/download/nginx-1.17.6.tar.gz"
RUN tar -zxvf nginx-1.17.6.tar.gz
WORKDIR nginx-1.17.6
RUN apt-get install build-essential -y
RUN apt-get install libpcre3-dev zlib1g-dev libssl-dev -y
RUN ./configure --prefix=/etc/nginx \
        --with-http_ssl_module \
		--with-http_realip_module \
        --with-http_stub_status_module \
        --without-http_proxy_module \
        --with-stream=dynamic
RUN make
RUN make install
RUN apt-get remove wget -y
RUN apt-get remove build-essential -y
RUN echo export PATH="$PATH:/etc/nginx/sbin/" >> ~/.bashrc
RUN export PATH=$path:/etc/nginx/sbin/
#CERTBOT
    #location ~ /.well-known/acme-challenge/ {
    #    return 200 'gangnam style!';
    #    add_header Content-Type text/plain;
    #}
    RUN apt-get install python-certbot-nginx -y
    #ex: certbot --nginx -d api.service.nplay.com.br

    #NGINX CONF
    COPY docker/nginx.conf /etc/nginx

    #Letsencrypt/Hosts folder
        RUN mkdir -p /etc/letsencrypt
        RUN echo '# This file contains important security parameters. If you modify this file\n# manually, Certbot will be unable to automatically provide future security\n# updates. Instead, Certbot will print and log an error message with a path to\n# the up-to-date file that you will need to refer to when manually updating\n# this file.\n\nssl_session_cache shared:le_nginx_SSL:1m;\nssl_session_timeout 1440m;\n\nssl_protocols TLSv1 TLSv1.1 TLSv1.2;\nssl_prefer_server_ciphers on;\n\nssl_ciphers "ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS";' > /etc/letsencrypt/options-ssl-nginx.conf
        RUN echo '-----BEGIN DH PARAMETERS-----\nMIIBCAKCAQEA//////////+t+FRYortKmq/cViAnPTzx2LnFg84tNpWp4TZBFGQz\n+8yTnc4kmz75fS/jY2MMddj2gbICrsRhetPfHtXV/WVhJDP1H18GbtCFY2VVPe0a\n87VXE15/V8k1mE8McODmi3fipona8+/och3xWKE2rec1MKzKT0g6eXq8CrGCsyT7\nYdEIqUuyyOP7uWrat2DX9GgdT0Kj3jlN9K5W7edjcrsZCwenyO4KbXCeAvzhzffi\n7MA0BM0oNC9hkXL+nOmFg/+OTxIy7vKBg8P+OxtMb61zO7X8vC7CIAXFjvGDfRaD\nssbzSibBsu/6iGtCOGEoXJf//////////wIBAg==\n-----END DH PARAMETERS-----' > /etc/letsencrypt/ssl-dhparams.pem
#END

#HOSTS
COPY docker/hosts/* /etc/nginx/sites-enabled/

#CERTS
COPY docker/letsencrypt /etc/letsencrypt/

RUN rm -rf docker

#CMD ["./etc/nginx/sbin/nginx", "-g", "'daemon off;'"]
#CMD ["nginx"]